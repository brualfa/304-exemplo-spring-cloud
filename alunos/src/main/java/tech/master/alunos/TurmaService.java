package tech.master.alunos;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feign.FeignException;

@Service
public class TurmaService {
  @Autowired
  private CursoClient cursoClient;
  @Autowired
  private TurmaRepository repository;

  Logger logger = LoggerFactory.getLogger(TurmaService.class);
  
  public Iterable<Turma> listar(){
    return repository.findAll();
  }
  
  public Turma criar(int idCurso) {
    try {
      Curso curso = cursoClient.buscar(idCurso);
      
      Turma turma = new Turma();
      turma.setNomeCurso(curso.getNome());
      turma.setDataInicio(LocalDate.now());
      
      return repository.save(turma);
    } catch (FeignException e) {
      logger.error(e.getMessage());
      return null;
    }
  }
}
